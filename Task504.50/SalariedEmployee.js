import Employee from './Employee.js';

class SalariedEmployee extends Employee {
  constructor(firstName, lastName, socialSecurityNumber, weeklySalary) {
    super(firstName, lastName, socialSecurityNumber);
    this.weeklySalary = weeklySalary;
  }
}

export default SalariedEmployee;
