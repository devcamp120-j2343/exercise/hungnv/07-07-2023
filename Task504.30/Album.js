import Product from './Product.js';

class Album extends Product {
    constructor(name, price, numberOfCopies) {
        super(name, price, numberOfCopies);
    }

    artist() {
        console.log(`Album: ${this.name} - Artist: Unknown`);
    }
}

export default Album;
