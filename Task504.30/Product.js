class Product {
    constructor(name, price, numberOfCopies) {
        this.name = name;
        this.price = price;
        this.numberOfCopies = numberOfCopies;
    }

    sellCopies() {
        console.log(`Selling copies of ${this.name}`);
    }

    orderCopies(num) {
        console.log(`Ordering ${num} copies of ${this.name}`);
    }
}

export default Product;
