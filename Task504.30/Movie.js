import Product from './Product.js';

class Movie extends Product {
    constructor(name, price, numberOfCopies) {
        super(name, price, numberOfCopies);
    }

    director() {
        console.log(`Movie: ${this.name} - Director: Unknown`);
    }
}

export default Movie;
