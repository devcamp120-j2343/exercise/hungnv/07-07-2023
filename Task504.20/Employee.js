import Person from './Person.js';

class Employee extends Person {
    constructor(personName, personAge, gender, employer, salary, position) {
        super(personName, personAge, gender);
        this.employer = employer;
        this.salary = salary;
        this.position = position;
    }

    getEmployeeInfo() {
        return `Name: ${this.personName}, Age: ${this.personAge}, Gender: ${this.gender}, Employer: ${this.employer}, Salary: ${this.salary}, Position: ${this.position}`;
    }

    getPosition() {
        return this.position;
    }

    setPosition(newPosition) {
        this.position = newPosition;
    }
}

export default Employee;
