import Person from './Person.js';

class Student extends Person {
    constructor(personName, personAge, gender, standard, collegeName, grade) {
        super(personName, personAge, gender);
        this.standard = standard;
        this.collegeName = collegeName;
        this.grade = grade;
    }

    getStudentInfo() {
        return `Name: ${this.personName}, Age: ${this.personAge}, Gender: ${this.gender}, Standard: ${this.standard}, College: ${this.collegeName}, Grade: ${this.grade}`;
    }

    getGrade() {
        return this.grade;
    }

    setGrade(newGrade) {
        this.grade = newGrade;
    }
}

export default Student;
