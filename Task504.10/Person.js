class Person {
    constructor(personName, personAge, gender) {
        this.personName = personName;
        this.personAge = personAge;
        this.gender = gender;
    }

    getPersonInfo() {
        return `Name: ${this.personName}, Age: ${this.personAge}, Gender: ${this.gender}`;
    }
}

export default Person;
